// Setup Express
const express = require('express');
const app = express();
const server = require("http").Server(app);
const path = require('path');


// --- Constants
// Server 
const PORT = 3000;
server.listen(PORT, () =>
    console.log(`L-Game listening on port ${PORT}!`)
);

// register static directories
app.use("/public", express.static(__dirname + "/public"));

// Routes
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/src/client/html/index.html'));
});
